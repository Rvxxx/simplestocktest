# The project

## stack:
### backend:
- JDK 17 (w/ preview feature switch-expression-pattern-matching)
- SpringBoot 2.7.0
### frontend:
- JavaScript ES7
- React 18.1
- Saas
 
## add dependencies:
- Lombok 1.18 (w/ @Jacksonized)
- Jackson (w/ datatype-jsr310 for LocalDate handling)
- H2 in memory DB
