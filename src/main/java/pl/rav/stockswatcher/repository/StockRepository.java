package pl.rav.stockswatcher.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.rav.stockswatcher.model.database.Stock;

import java.math.BigDecimal;
import java.util.List;

public interface StockRepository extends CrudRepository<Stock, Long> {

    @Query(value = "SELECT price FROM stock WHERE date BETWEEN ?1 AND ?2", nativeQuery = true)
    List<BigDecimal> findAllPricesBetweenDates(String start, String end);

    @Query(value = "SELECT * FROM stock WHERE symbol = ?1 AND date BETWEEN ?2 AND ?3", nativeQuery = true)
    List<Stock> findStocksBetweenDates(String symbol, String start, String end);

    /** Be careful! case-sensitive! */
    boolean existsBySymbol(String symbol);

    /** Be careful! case-sensitive! */
    @Query(value = "SELECT COUNT(symbol) FROM stock WHERE symbol = ?1", nativeQuery = true)
    int getCountBYStock(String symbol);

}
