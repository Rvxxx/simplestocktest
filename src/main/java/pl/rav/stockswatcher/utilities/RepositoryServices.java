package pl.rav.stockswatcher.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rav.stockswatcher.model.database.Stock;
import pl.rav.stockswatcher.model.incoming.StockData;
import pl.rav.stockswatcher.repository.StockRepository;

import java.util.List;

@Service
public class RepositoryServices {

    @Autowired
    StockRepository repository;

    public List<Stock> getPricesBetweenDatesSortedAsc(String symbol, String start, String end) {
        List<Stock> stocks = repository.findStocksBetweenDates(symbol, start, end);
        return stocks.stream().sorted().toList();
    }

    public void saveValuesToDatabase(StockData stockData) {
        String symbol = stockData.getMetaData().getSymbol().toUpperCase();

        stockData.getTimeSeries()
                .getSeries()
                .forEach((date, prices) -> {
                    Stock stock = Stock.builder()
                            .symbol(symbol)
                            .date(date)
                            .price(prices.getClose())
                            .build();

                    repository.save(stock);
                });
    }

}
