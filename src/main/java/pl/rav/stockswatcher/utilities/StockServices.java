package pl.rav.stockswatcher.utilities;

import pl.rav.stockswatcher.model.database.Stock;

import java.math.BigDecimal;
import java.util.List;

// TODO: Add AOP on the errors/logging regarding empty collection
public class StockServices {

    public static BigDecimal getBestPriceToBuy(List<Stock> stocks) {
        if (stocks.isEmpty()) {
            throw new IllegalArgumentException("Stocks collection was empty, error on getting the best buying price");
        }
        return getFirstStock(stocks).getPrice();
    }

    public static BigDecimal getBestPriceToSell(List<Stock> stocks) {
        if (stocks.isEmpty()) {
            throw new IllegalArgumentException("Stocks collection was empty, error on getting the best selling price");
        }

        return getLastStock(stocks).getPrice();
    }

    public static String getBestDateToBuy(List<Stock> stocks) {
        if (stocks.isEmpty()) {
            throw new IllegalArgumentException("Stocks collection was empty, error on getting the best buy date");
        }

        return getFirstStock(stocks).getDate();
    }

    public static String getBestDateToSell(List<Stock> stocks) {
        if (stocks.isEmpty()) {
            throw new IllegalArgumentException("Stocks collection was empty, error on getting the best selling date");
        }

        return getLastStock(stocks).getDate();
    }

    private static Stock getFirstStock(List<Stock> stocks) {
        return stocks.get(0);
    }

    private static Stock getLastStock(List<Stock> stocks) {
        return stocks.get(stocks.size() - 1);
    }

}
