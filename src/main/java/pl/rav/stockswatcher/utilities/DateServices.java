package pl.rav.stockswatcher.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;
import pl.rav.stockswatcher.model.incoming.StockData;
import pl.rav.stockswatcher.model.outgoing.Result;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static pl.rav.stockswatcher.configuration.ConstConfig.*;

public class DateServices {

    /**
     * @param date String date in the format: yyyy-MM-dd
     */
    public static long getEpochTimestampFromDate(String date) {
        return LocalDate.parse(date).toEpochDay();
    }

    /**
     * @param epochDaysCount date counts starting from 1970-01-01 => 0L
     */
    public static LocalDate getDateFromEpochTimestamp(long epochDaysCount) {
        return LocalDate.ofEpochDay(epochDaysCount);
    }

    // TODO: improve implementation, this one was quick one to move forward...
    public static boolean areDatesAppropriate(String from, String to) {
        if (!isDateValid(from) || !isDateValid(to)) {
//            throw new IllegalArgumentException("Format of the date(s) is/are invalid, please check the endpoint URL");
            return false;
        }

        Date fromDate, toDate;

        try {
            fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(from);
            toDate = new SimpleDateFormat("yyyy-MM-dd").parse(to);

        } catch (ParseException e) {
//            throw new IllegalArgumentException("Format of the date(s) is/are invalid, cannot parse, please check the endpoint URL");
            return false;
        }

        return fromDate.before(toDate);
    }

    protected static boolean isDateValid(String date) {
        return date.matches(ACCEPTABLE_DATE_FORMAT);
    }

}
