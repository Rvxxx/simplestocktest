package pl.rav.stockswatcher.utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;
import pl.rav.stockswatcher.model.incoming.StockData;

import static pl.rav.stockswatcher.configuration.ConstConfig.API_KEY;
import static pl.rav.stockswatcher.configuration.ConstConfig.URI_EXTERNAL_DAILY_API;

public class SerialisationServices {

    // TODO: make it loosely-coupled
    // TODO: log the error by AspectJ / should I throw thw error here ? or pass it up ?

    public static StockData convertJsonStringToJavaObject(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        StockData stock;

        try {
            stock = objectMapper.readValue(json, StockData.class);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Cannot read and/or parse the JSON data (" + e.getMessage() + ")");
        }

        return stock;
    }

    public static String toResultJson(Object result) {
        ObjectMapper mapper = new ObjectMapper();
        String json;

        try {
            json = mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Error while trying to Jacksonized the object: " + result);
        }

        return json;
    }

    // TODO: make it loosely-coupled
    // TODO: add &outputsize=full for the production code
    public static String getAlphaVantageStockJsonString(String symbol) {
        String externalApiUrl = URI_EXTERNAL_DAILY_API + "&symbol=" + symbol + "&apikey=" + API_KEY;

        return new RestTemplate().getForObject(externalApiUrl, String.class);
    }

}
