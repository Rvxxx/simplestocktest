package pl.rav.stockswatcher.model.incoming;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Prices {

    @JsonProperty("1. open")
    private BigDecimal open;
    @JsonProperty("2. high")
    private BigDecimal high;
    @JsonProperty("3. low")
    private BigDecimal low;
    @JsonProperty("4. close")
    private BigDecimal close;
    @JsonProperty("5. volume")
    private BigDecimal volume;

}
