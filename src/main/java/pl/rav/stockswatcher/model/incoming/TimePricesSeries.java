package pl.rav.stockswatcher.model.incoming;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TimePricesSeries {

    // TODO: Map key should be LocalDate... however need add dependency and config... to be improved on the next release.
    @JsonProperty("map")
    private Map<String, Prices> series;

    @JsonCreator
    public TimePricesSeries(Map<String, Prices> series) {
        this.series = series;
    }

}
