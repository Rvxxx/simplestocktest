package pl.rav.stockswatcher.model.database;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stock implements Comparable<Stock> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * in the first, test release, have uploaded all data into one table... next release, split into separate tables for each stock/symbol
     */
    @Setter
    private String symbol;

    @Setter
    private String date;
    @Setter
    private BigDecimal price;

    public Stock(String symbol, String date, BigDecimal price) {
        this.symbol = symbol;
        this.date = date;
        this.price = price;
    }

    @Override
    public int compareTo(Stock stock) {
        return price.compareTo(stock.getPrice());
    }

    @Override
    public String toString() {
        return '{' + date + " : " + price + '}';
    }

}
