package pl.rav.stockswatcher.model.outgoing;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Builder
@Getter
@ToString
public class Result {

    private String symbol;
    private String buyDate;
    private String sellDate;
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;
    private BigDecimal profit;

}
