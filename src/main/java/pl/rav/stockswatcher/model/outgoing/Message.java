package pl.rav.stockswatcher.model.outgoing;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class Message {

    private String message;

}
