package pl.rav.stockswatcher.configuration;

public class ConstConfig {

    // TODO: add option in the next release (without this only 100 last data series are fetch, with this 20+yrs of data is fetched - it take some time) - thus allow option if not needed
    public static final String FULL_DATA_SET = "&outputsize=full";

    public static final String URI_EXTERNAL_DAILY_API = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY" + FULL_DATA_SET;

    // TODO: move key to the properties file
    public static final String API_KEY = "KQYA0T51E3E1GC29";

    public static final String ACCEPTABLE_DATE_FORMAT = "^((19|2[0-9])[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";

}
