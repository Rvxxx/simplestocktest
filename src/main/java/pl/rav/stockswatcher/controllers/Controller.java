package pl.rav.stockswatcher.controllers;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.rav.stockswatcher.model.database.Stock;
import pl.rav.stockswatcher.model.incoming.StockData;
import pl.rav.stockswatcher.model.outgoing.Message;
import pl.rav.stockswatcher.model.outgoing.Result;
import pl.rav.stockswatcher.repository.StockRepository;
import pl.rav.stockswatcher.utilities.RepositoryServices;

import java.util.List;

import static pl.rav.stockswatcher.utilities.SerialisationServices.*;
import static pl.rav.stockswatcher.utilities.StockServices.*;
import static pl.rav.stockswatcher.utilities.DateServices.*;

@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:3000/")
public class Controller {

    @Autowired
    StockRepository repository;

    @Autowired
    RepositoryServices repositoryServices;

    // TODO: html for the error
    // TODO: change banner for spring
    // TODO: extract API key to properties
    // TODO: clean the getAlphaVantage and make DRY & separate the concerns
    // TODO: tak away logging by AspectJ (logging is not the method responsibility)

    /**
     * Fetch Alpha Vantage stock data and upload time-pricing series to H2 database
     *
     * @param symbol stock exchange trading symbol, ex. IBM
     * @return are data fetched
     */
    @GetMapping("/fetch/{symbol}")
    public ResponseEntity<String> fetchAlphaVantageData(@PathVariable String symbol) {
        String stockSymbol = symbol.toUpperCase();
        ResponseEntity<String> response;
        Message message;

        if (repository.existsBySymbol(stockSymbol)) {
            message = Message.builder()
                    .message("Data for '" + stockSymbol + "' are already fetched from the Alpha Vantage server, no need to fetch again")
                    .build();

            response = ResponseEntity
                    .status(HttpStatus.ALREADY_REPORTED)
                    .body(toResultJson(message));
            log.warn(response.getBody());

            return response;
        }

        String json = getAlphaVantageStockJsonString(stockSymbol);
        StockData stockData = convertJsonStringToJavaObject(json);
        repositoryServices.saveValuesToDatabase(stockData);

        message = Message.builder()
                .message("Data for '" + stockSymbol + "' was fetched and saved to the database")
                .build();

        response = ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(toResultJson(message));

        log.info(response.getBody()); // TODO: logging to be done by AOP

        return response;
    }


    /**
     * Get best buy/sell pricing/dates for particular stock based on data from H2 (data needs to be fetched first)
     *
     * @param symbol stock exchange trading symbol, ex. IBM
     * @param from   date in the YYYY-MM-DD format (beginning period, needs to be before 'to'), ex. 2022-05-27
     * @param to     date in the YYYY-MM-DD format (end period, needs to be after 'from'), ex. 2022-06-01
     * @return Result JSON
     */
    @GetMapping("/{symbol}")
    public ResponseEntity<String> getTimePricingProfit(@PathVariable String symbol, @RequestParam String from, @RequestParam String to) {
        ResponseEntity<String> response;
        Message message;

        if (!areDatesAppropriate(from, to)) {
            message = Message.builder()
                    .message("Can not proceed with the request as date(s) is/are invalid, pleas check endpoint")
                    .build();

            response = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(toResultJson(message));

            log.error(response.getBody()); // TODO: logging to be done by AOP

            return response;
        }

        String stockSymbol = symbol.toUpperCase();

        if (!repository.existsBySymbol(stockSymbol)) {
            message = Message.builder()
                    .message("Data for '" + stockSymbol + "' are not available in the database, please do the fetch first")
                    .build();

            response = ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(toResultJson(message));

            log.warn(response.getBody());  // TODO: logging to be done by AOP

            return response;
        }

        List<Stock> stocks = repositoryServices.getPricesBetweenDatesSortedAsc(stockSymbol, from, to);

        Result result = Result.builder()
                .symbol(stockSymbol)
                .buyDate(getBestDateToBuy(stocks))
                .buyPrice(getBestPriceToBuy(stocks))
                .sellDate(getBestDateToSell(stocks))
                .sellPrice(getBestPriceToSell(stocks))
                .profit(getBestPriceToSell(stocks).subtract(getBestPriceToBuy(stocks)))
                .build();

        response = ResponseEntity.status(HttpStatus.ACCEPTED).body(toResultJson(result));

        log.info(response.getBody());  // TODO: logging to be done by AOP

        return response;
    }

}
