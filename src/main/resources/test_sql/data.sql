DROP TABLE IF EXISTS test;

CREATE TABLE test
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    date  DATE            NOT NULL,
    price DECIMAL(250, 4) NOT NULL
);

-- insert test
INSERT INTO test (date, price)
VALUES ('2022-06-03', 123.4567);
