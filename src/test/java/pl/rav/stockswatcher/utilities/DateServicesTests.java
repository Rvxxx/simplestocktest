package pl.rav.stockswatcher.utilities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static pl.rav.stockswatcher.utilities.DateServices.*;

public class DateServicesTests {

    @Test
    public void getEpochTimestampFromDateTest() {
        Assertions.assertEquals(getEpochTimestampFromDate("1970-01-01"), 0L);
        Assertions.assertEquals(getEpochTimestampFromDate("1970-01-31"), 30L);
        Assertions.assertEquals(getEpochTimestampFromDate("2022-06-01"), 19144L);
        Assertions.assertEquals(getEpochTimestampFromDate("2022-06-02"), 19145L);
    }

    @Test
    public void getDateFromEpochTimestampTest() {
        Assertions.assertEquals(getDateFromEpochTimestamp(0), LocalDate.of(1970,1,1));
        Assertions.assertEquals(getDateFromEpochTimestamp(19145L), LocalDate.of(2022,6,2));
    }

    @Test
    public void isDateValidTest() {
        Assertions.assertTrue(isDateValid("2022-05-27"));
        Assertions.assertTrue(isDateValid("2000-01-01"));
        Assertions.assertFalse(isDateValid("2001-01-32"));
        Assertions.assertFalse(isDateValid("2Y01-01-32"));
        Assertions.assertFalse(isDateValid("201-01-29"));
        Assertions.assertFalse(isDateValid("202205-27"));
        Assertions.assertFalse(isDateValid("20220527"));
    }

    @Test
    public void areDatesAppropriateTest() {
        Assertions.assertTrue(areDatesAppropriate("2022-05-27","2022-06-01"));
        Assertions.assertFalse(areDatesAppropriate("2022-06-02","2022-06-01"));
        Assertions.assertFalse(areDatesAppropriate("2022-06-02","2022-06-02"));
    }

}
